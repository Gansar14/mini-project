<?php

Route::namespace('Auth')->group(function(){
    
    Route::post('register','RegisterController');
    Route::post('login','LoginController');
    Route::post('logout','LogoutController');
});

Route::namespace('Siakad')->middleware('auth:api')->group(function(){
    
    Route::post('profile','ProfileController@store');
    Route::post('daftar-matkul','DaftarmatkulController@store');
    
    Route::patch('update-profile/{profile}','ProfileController@update');
    Route::patch('update-daftarmatkul/{daftarmatkul}','DaftarmatkulController@update');
});

Route::get('profiles/{profile}', 'Siakad\ProfileController@show');
Route::get('daftar-matkul/{daftarmatkul}', 'Siakad\DaftarmatkulController@show');

Route::get('matakuliah', 'Siakad\MatakuliahController@index');


Route::get('user','UserController');


