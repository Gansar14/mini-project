<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DaftarmatkulResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'User'=> $this->user,
            'tanggal isi' => $this->tanggal_isi,
            'respon waldos' =>  $this->respon_waldos,
            'daftar matkul' =>$this->matkul_id,
            'matkul'=> $this->matkul,
            
            
        ];
    }

    public function with($request)
    {
        return ['status'=>'sucess'];
    }
}
