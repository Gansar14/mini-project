<?php

namespace App\Http\Controllers\Siakad;
use App\Models\Siakad\Daftarmatkul;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\DaftarmatkulResource;

class DaftarmatkulController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_isi'=> ['required'],
            'respon_waldos'=>['required'],
            'matkul'=>['required'],
        ]);

        $daftamatkul = auth()->user()->daftarmatkuls()->create($this->daftarmatkulStore());
        
        return $daftamatkul;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(daftarmatkul $daftarmatkul)
    {
       return new DaftarmatkulResource($daftarmatkul);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Daftarmatkul $daftarmatkul)
    {
        $daftarmatkul->update($this->daftarmatkulStore());
        return new DaftarmatkulResource($daftarmatkul);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function daftarmatkulStore()
    {
      return [
          'tanggal_isi'=> request('tanggal_isi'),
          'respon_waldos' => request('respon_waldos'),  
          'matkul_id'=> request('matkul'),
      ];
    }
    
}
