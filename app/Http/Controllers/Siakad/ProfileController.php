<?php

namespace App\Http\Controllers\Siakad;

use App\Models\Siakad\Profile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ProfileResource;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nim'=> ['required', 'max:8'],
            'fakultas'=>['required'],
            'jurusan'=>['required'],
            'semester'=>['required'],
            'ipk'=>['required'],

        ]);

        $profile = auth()->user()->profiles()->create($this->profileStore());

        return $profile;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        return new ProfileResource($profile);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $profile->update($this->profileStore());
        return new ProfileResource($profile);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profileStore()
    {
      return [
            'nim'=> request('nim'),
            'fakultas' => request('fakultas'),
            'jurusan'=> request('jurusan'),
            'semester' => request('semester'),
            'ipk' => request('ipk'),
      ];
    }
}
