<?php

namespace App\Models\Siakad;

use Illuminate\Database\Eloquent\Model;

class Matkul extends Model
{
    public function daftarmatkuls()
    {
        return $this->hasMany(Daftarmatkul::class);
    }
}
