<?php

namespace App\Models\Siakad;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable =['nim','fakultas','jurusan','semester','ipk'];

    // protected $with =['user'];
    public function getRouteKeyName()
    {
        return 'nim';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
