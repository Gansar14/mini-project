<?php

namespace App\Models\Siakad;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Daftarmatkul extends Model
{
    protected $fillable =['tanggal_isi','respon_waldos','matkul_id'];

    public function getRouteKeyName()
    {
        return 'user_id';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function matkul()
    {
        return $this->belongsTo(Matkul::class);
    }
    
}
